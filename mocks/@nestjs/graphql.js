const decoratorHelper = () => () => null;
const typeHelper = (classRef) => classRef;

export const ID = null;

export const Field = decoratorHelper;
export const InputType = decoratorHelper;
export const ObjectType = decoratorHelper;

export const IntersectionType = typeHelper;
export const OmitType = typeHelper;
export const PartialType = typeHelper;
export const PickType = typeHelper;
