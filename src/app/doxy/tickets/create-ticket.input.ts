import { InputType } from '@nestjs/graphql';
import { Ticket } from './ticket.entity.js';

@InputType()
export class CreateTicketInput extends Ticket.CreateInput(['id']) {}
