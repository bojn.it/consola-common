const decoratorHelper = () => () => null;

export const Column = decoratorHelper;
export const Entity = decoratorHelper;
export const PrimaryGeneratedColumn = decoratorHelper;
