import { InputType } from '@nestjs/graphql';
import { Ticket } from './ticket.entity.js';

@InputType()
export class UpdateTicketInput extends Ticket.UpdateInput(['id']) {}
