import { ObjectType } from '@nestjs/graphql';
import { Ticket } from './ticket.entity.js';

@ObjectType()
export class TicketOutput extends Ticket.Output([]) {}
