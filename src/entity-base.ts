import type { Type } from '@nestjs/common/interfaces/type.interface.js'
import { InputType } from '@nestjs/graphql'
import { ObjectType } from '@nestjs/graphql'
import { OmitType } from '@nestjs/graphql'
import { PartialType } from '@nestjs/graphql'
import { instanceToInstance } from 'class-transformer'
import type { ValueOf } from 'ts-essentials'

export const C: ['C'] = ['C']
export const R: ['R'] = ['R']
export const U: ['U'] = ['U']
export const CU: ['C', 'U'] = ['C', 'U']
export const RU: ['R', 'U'] = ['R', 'U']
export const CRU: ['C', 'R', 'U'] = ['C', 'R', 'U']

const entityBaseKeysToOmit: readonly EntityBaseKeysToOmit[] = ['toOutput']
export type EntityBaseKeysToOmit = ValueOf<keyof EntityBase>

export abstract class EntityBase<T extends Partial<Omit<EntityBase, any>> = any> {
  static CreateInput<
    T extends EntityBase,
    K extends Exclude<keyof T, keyof EntityBase>
  >(
    this: Type<T>,
    keysToOmit: readonly K[],
  ): Type<Omit<
    T,
    typeof keysToOmit[number] | EntityBaseKeysToOmit
  >> {
    return OmitType(this, [
      ...keysToOmit,
      ...entityBaseKeysToOmit,
    ], InputType)
  }

  static UpdateInput<
    T extends EntityBase,
    K extends Exclude<keyof T, keyof EntityBase>
  >(
    this: Type<T>,
    keysToOmit: readonly K[],
  ): Type<Partial<Omit<
    T,
    typeof keysToOmit[number] | EntityBaseKeysToOmit
  >>> {
    return PartialType(OmitType(this, [
      ...keysToOmit,
      ...entityBaseKeysToOmit,
    ], InputType))
  }

  static Output<
    T extends EntityBase,
    K extends Exclude<keyof T, keyof EntityBase>
  >(
    this: Type<T>,
    keysToOmit: readonly K[],
  ): Type<Partial<Omit<
    T,
    typeof keysToOmit[number] | EntityBaseKeysToOmit
  >>> {
    return PartialType(OmitType(this, [
      ...keysToOmit,
      ...entityBaseKeysToOmit,
    ], ObjectType))
  }

  toOutput(this: T, groups: Suggest<'*' | 'C' | 'U'>[]): T {
    return instanceToInstance(this, { strategy: 'excludeAll', groups })
  }
}
