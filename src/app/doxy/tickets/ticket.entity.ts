import { Forma } from '@bojnit/forma-common'
import { IsNotRequired } from '@bojnit/forma-common/class-validator'
import { IsRequired } from '@bojnit/forma-common/class-validator'
import { IsRequiredIfPresent } from '@bojnit/forma-common/class-validator'
import { Field } from '@nestjs/graphql'
import { ID } from '@nestjs/graphql'
import { Expose } from 'class-transformer'
import { IsUUID } from 'class-validator'
import { Length } from 'class-validator'
import { Column } from 'typeorm'
import { Entity } from 'typeorm'
import { PrimaryGeneratedColumn } from 'typeorm'
import { C } from '#src/entity-base'
import { CRU } from '#src/entity-base'
import { CU } from '#src/entity-base'
import { EntityBase } from '#src/entity-base'
import { U } from '#src/entity-base'
import type { TicketOutput } from './ticket.output.js'

@Entity('tickets')
export class Ticket extends EntityBase<TicketOutput> {
  @PrimaryGeneratedColumn('uuid')
  @Field(type => ID)
  @IsUUID(4, { always: true })
  @Expose({ groups: CRU })
  readonly id!: string

  @Column({ length: 255 })
  @Field()
  @Forma()
  @IsRequired({ groups: C })
  @IsRequiredIfPresent({ groups: U })
  @Length(1, 255)
  @Expose({ groups: CRU })
  title!: string

  @Column('text')
  @Field({ nullable: true })
  @Forma(type => String)
  @IsNotRequired({ groups: CU })
  @Length(0, 65535)
  @Expose({ groups: CRU })
  description!: string | null
}
